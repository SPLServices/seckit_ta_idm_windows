#
# Determine the health and statistics of this Microsoft DNS Server
#
$Output = New-Object System.Collections.ArrayList
$Date = Get-Date -format 'yyyy-MM-ddTHH:mm:sszzz'
write-host -NoNewline ""$Date

# Name of Server
$ServerName = $env:ComputerName
write-host -NoNewline ""Server=`"$ServerName`"

Get-NetConnectionProfile -NetworkCategory DomainAuthenticated | ForEach-Object {
    $dict = [ORDERED]@{}
    $dict.Add('Domain',$_.Name)
    $dict.Add('InterfaceAlias',$_.InterfaceAlias)
    $dict.Add('InterfaceIndex',$_.InterfaceIndex)
    $dict.Add('NetworkCategory',$_.NetworkCategory)
    $dict.Add('IPv4Connectivity',$_.IPv4Connectivity)
    $dict.Add('IPv6Connectivity',$_.IPv6Connectivity)

    $adapter = Get-NetAdapter -InterfaceIndex $_.InterfaceIndex
    $dict.Add('InterfaceDescription',$adapter.InterfaceDescription)
    $dict.Add('Status',$adapter.Status)
    $dict.Add('MacAddress',($adapter.MacAddress -replace "-",":").ToLower())
    $dict.Add('LinkSpeed',$adapter.LinkSpeed)


    Get-NetIPConfiguration -InterfaceIndex $_.InterfaceIndex | ForEach-Object {
        Get-NetIPAddress -InterfaceIndex $_.InterfaceIndex -AddressFamily IPv4 | ForEach-Object {
          $dict.Add('IPv4Address',$_.IPAddress)
          $dict.Add('IPv4PrefixLength',$_.PrefixLength)
          $dict.Add('IPv4PrefixOrigin',$_.PrefixOrigin)
          $dict.Add('IPv4SuffixOrigin',$_.SuffixOrigin)
          $dict.Add('IPv4AddressState',$_.AddressState)
          $dict.Add('IPv4PreferredLifetime',$_.PreferredLifetime)
          $dict.Add('IPv4SkipAsSource',$_.SkipAsSource)
          $dict.Add('IPv4PolicyStore',$_.PolicyStore)

        }
        Get-DnsClientServerAddress -InterfaceIndex $_.InterfaceIndex -AddressFamily IPv4  | ForEach-Object {
                      $dict.Add('IPv4DNS',$_.ServerAddresses)
        }
        Get-NetIPAddress -InterfaceIndex $_.InterfaceIndex -AddressFamily IPv6 | ForEach-Object {
          $dict.Add('IPv6Address',$_.IPAddress)
          $dict.Add('IPv6PrefixLength',$_.PrefixLength)
          $dict.Add('IPv6PrefixOrigin',$_.PrefixOrigin)
          $dict.Add('IPv6SuffixOrigin',$_.SuffixOrigin)
          $dict.Add('IPv6AddressState',$_.AddressState)
          $dict.Add('IPv6ValidLifetime',$_.ValidLifetime)
          $dict.Add('IPv6PreferredLifetime',$_.PreferredLifetime)
          $dict.Add('IPv6SkipAsSource',$_.SkipAsSource)
          $dict.Add('IPv6PolicyStore',$_.PolicyStore)

        }
        Get-DnsClientServerAddress -InterfaceIndex $_.InterfaceIndex -AddressFamily IPv6  | ForEach-Object {
                $dict.Add('IPv6DNS',$_.ServerAddresses)
        }
        $alias = $_.InterfaceAlias
        $interface = [Net.NetworkInformation.NetworkInterface]::GetAllNetworkInterfaces() | Where-Object {$_.Name -eq $alias}
        $dhcpservers = $interface.GetIPProperties().DhcpServerAddresses.IPAddressToString

        $dict.Add('DHCPServers',$dhcpservers)
    }
    foreach ($d in $dict.GetEnumerator()) {
     Write-Host -NoNewline " $($d.Name)=""$($d.Value)"""
    }
}
