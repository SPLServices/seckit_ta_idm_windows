.. SecKit SA IDM Windows documentation master file, created by


==================================================
Welcome to SecKit TA IDM Windows's documentation!
==================================================

Success Enablement Content "SecKit" apps for Splunk are designed
to accelerate the tedious or difficult tasks. This application TA IDM Windows
is an add on for Splunk Enterprise designed to identify and enrich
asset and identity information based by collection of specific information from the Windows Operating System.

- What is the static IP configuration of the host?
- Which interfaces are connected to domain networks?
- What DHCP and DNS servers are providing IPAM and DNS services for this system?

Before you get started
======================

- Complete Splunk Enterprise Security Administration training
- Deploy the Splunk Universal forwarder to all monitored Windows Servers and Endpoints
- Configure data collection for Windows to support the Security Monitoring and Investigation responsibilities of the organization. Review and apply the guidance as appropriate for your organization for Splunk TA Windows :doc:`SecKit TA <ta:index>`

Support
======================

- Reporting issues or requesting enhancements `Issue Tracker <https://bitbucket.org/SPLServices/seckit_ta_idm_windows/issues?status=new&status=open>`_
- `Source <https://bitbucket.org/SPLServices/seckit_ta_idm_windows>`_


Documentation
======================

.. toctree::
   :maxdepth: 2
   :glob:

   requirements
   install/install
